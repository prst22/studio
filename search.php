<?php
/**
 * The template for displaying search results pages
 */

get_header(); ?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="container single_page">
                	<div class="row">
						<header class="mb-5 col-12 page_header">
							<?php if ( have_posts() ) : ?>
								<h1 class="page_title"><?php printf( __( 'Search Results for: %s', 'studio' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
							<?php else : ?>
								<h1 class="page_title"><?php _e( 'Nothing Found', 'studio' ); ?></h1>
							<?php endif; ?>
						</header><!-- .page-header -->
					</div>
					<?php
					if ( have_posts() ) :
						/* Start the Loop */
						while ( have_posts() ) : the_post();?>
                           
                           <?php if ( has_post_thumbnail() && get_post_type( get_the_ID() ) == 'post' ): ?>

							<div class="row">
								<div class="col-12">

									<h4 class="mb-0">
										<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									</h4>
								    <?php $postType = get_post_type_object(get_post_type());
								    if ($postType) :?>
								    <span class="serch_cat_info">
								    	<a href="<?php echo get_page_link( get_page_by_path( 'all-news' )->ID ); ?>">	
											<?php echo esc_html($postType->labels->singular_name) . ', '; ?>
									    </a>
									    <?php endif; ?>												     	        	
										<?php the_time('j F, Y'); ?>
									</span>
								</div>

								<div class="col-12 mb-3 mb-md-4">

								    <figure class="float-left mb-0 mb-lg-2 mr-2 cat_serch_image_cnt">
										<a href="<?php the_permalink() ?>" class="d-block position-relative cat_serch_image_cnt__link_cnt">
											<?php the_post_thumbnail('small-thumnail',
												$attr = array('alt'   => "Search image"));?>
												
										</a>
									</figure>	
									<p class="serch_cat_exc"><?php the_excerpt_rss(); ?></p>
								    
							    </div>
						    </div>

						    <?php elseif( has_post_thumbnail() && (get_post_type( get_the_ID() ) == 'stories' || 'studio_clients') ): ?>

						    <div class="row">
						    	<div class="col-12 mb-3 mb-md-4">

								    <figure class="float-left mb-0 mb-lg-2 mr-2 cat_serch_image_cnt">
										<a href="<?php the_permalink() ?>" class="d-block position-relative cat_serch_image_cnt__link_cnt">
											<?php the_post_thumbnail('small-thumnail',
												$attr = array('alt'   => "Search image"));?>
											<div class="position-absolute d-flex justify-content-center align-items-center overlay">
												<svg class="icon play"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#play"></use></svg>
											</div>

										</a>
									</figure>

									<?php $postType = get_post_type_object(get_post_type());
								    if ($postType) :?>
								    <span class="serch_cat_info">
                                        <!-- print post type link start -->
                                        <?php if(get_post_type( get_the_ID() ) == 'stories'): ?>
									    	<a href="<?php echo get_page_link( get_page_by_path( 'all-stories' )->ID ); ?>">	
												<?php echo esc_html($postType->labels->singular_name) . ', '; ?>
										    </a>
                                        <?php else: ?>
                                        	<a href="<?php echo get_page_link( get_page_by_path( 'all-clients' )->ID ); ?>">	
												<?php echo esc_html($postType->labels->singular_name) . ', '; ?>
										    </a>

                                        <?php endif; ?>
									    <!-- print post type link end -->

								    <?php endif; ?>												     	        	
										<?php the_time('j F, Y'); ?>
									</span>	
								    <h5 class="serch_cat_heading"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
								   
							    </div>
						    </div>

						    <?php elseif( !(get_post_type( get_the_ID() ) == 'stories' || 'studio_clients' || 'post')): ?>

					    	<div class="row">
						    	<div class="col-12 mb-3 mb-md-4">
						    		
								    <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
								    <?php the_excerpt(); ?>
						    	</div>
						    </div>


						<?php endif; ?>
			                
						<?php endwhile; // End of the loop.

					else : ?>

						<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'studio' ); ?></p>
						<?php endif;?>
					<?php if(is_paginated()): ?>										
						<div class="row">
							<div class="col-12 py-3">
								<div class="pagination_cnt">
									<div class="d-flex justify-content-between pagination_cnt__inner">
										<?php previous_posts_link( '<span class="iconslider icon-arrow-left2 px-1"></span>prev page '); ?>
								        <?php next_posts_link( 'next page <span class="iconslider icon-arrow-right2 px-1"></span>'); ?>
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer();
