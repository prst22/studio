<?php get_header();?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="single_page">
                    <?php while ( have_posts() ) : the_post(); ?>
                     
                        <div class="mb-5 single_page__post_heading">
                            <h1 class="heading_title"><?php the_title(); ?></h1>
                        </div>
                        <div class="content_here">
                        <?php  the_content();  ?>
                        </div>

                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer();?>