<?php
	get_header();
?>
	<?php if(is_home()): ?>
	<section class="block position-relative home_bg">	
		<div class="container">
			<div class="row">
				<div class="col-12 px-0">
					<div class="index_top_slider">
						<div class="owl-carousel position-relative">
							<?php
								$args = array( 
									'post_type' => 'studio_clients', 
									'posts_per_page' => 5,
									'no_found_rows' => true, 
									'update_post_meta_cache' => false, 
									'update_post_term_cache' => false, 
									'fields' => 'ids'
									 );
								$loop = new WP_Query( $args );
								if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();?>	
									<div class="position-relative owl-carousel__item slider">
										<div class="vertical_social">
											<ul class="vertical_social__list">
												<?php $fb = get_post_meta( get_the_ID(), 'studio_url_1', true );
												    if ($fb): ?>
														<li>
														    <a href="<?php echo $fb; ?>"><svg class="icon facebook2"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#facebook"></use></svg></a>
													    </li>
												<?php endif; ?>
												<?php $instagram = get_post_meta( get_the_ID(), 'studio_url_2', true ); 
												    if ($instagram): ?>
														<li>
														    <a href="<?php echo $instagram; ?>"><svg class="icon instagram"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#instagram"></use></svg></a>
													    </li>
												<?php endif; ?>
												<?php $youtube = get_post_meta( get_the_ID(), 'studio_url_3', true ); 
												    if ($youtube): ?>
														<li>
														    <a href="<?php echo $youtube; ?>"><svg class="icon youtube"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#youtube"></use></svg></a>
													    </li>
												<?php endif; ?>
												<?php $twitter = get_post_meta( get_the_ID(), 'studio_url_4', true ); 
												    if ($twitter): ?>
														<li>
														    <a href="<?php echo $twitter; ?>"><svg class="icon twitter"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#twitter"></use></svg></a>
													    </li>
												<?php endif; ?>
											</ul>
											<span class="slider_number"></span>
										</div>
										<a class="position-relative slider__item_link" href="<?php the_permalink(); ?>">
											<header class="position-absolute slider_header">
												<h1><?php the_title();?> </h1>
												<?php $description = get_post_meta( get_the_ID(), 'studio_text', true )?>
												<?php if($description): ?>
												<h5>
													 
														<?php echo $description; ?> 

												</h5>
												<?php endif; ?>
											</header>
											<?php the_post_thumbnail('large-client-thumnail',
												$attr = array(
												    'class' => "owl-lazy",
												    'alt'   => "home slider image",
													'data-src' => get_the_post_thumbnail_url( get_the_ID(), 'large-client-thumnail')));
											?>	
										</a>
										<a href="<?php the_permalink(); ?>" class="view_profile_btn">VIEW PROFILE</a>
									</div>
									
		                    <?php  
							/* Restore original Post Data */
								wp_reset_postdata();
								endwhile;
								else: ?>
                                   <h1>No clients so far(</h1>
							<?php endif;?>
						</div>
					</div>
				</div>
			</div>		
		</div>	
	</section>
	<?php endif; ?>
	<section class="block block--tabs_cnt">
		<div class="container position-relative mx-auto tabs_inner">
			<div class="row">
				<div class="col-12">
					<ul class="nav d-flex justify-content-center tabs_controlls" id="pills-tab" role="tablist">
						<?php $custom_terms = get_terms('client_occupation');
						  	    foreach($custom_terms as $key=>$value) {?>
						  	    	<?php if($key == 0): ?>
							  	    	<li class="nav-item text-uppercase">
							  	    		<a class="nav-link active" id="pills-<?php echo $value->slug; ?>-tab" data-toggle="pill" href="#pills-<?php echo $value->slug; ?>" role="tab" aria-controls="pills-home" aria-selected="true">
									  	    	<?php echo $value->slug ?>
									  	    </a>
								  	    </li>
								  	<?php  
								    elseif($key < 3): ?>
								    	<li class="nav-item text-uppercase">
							  	    		<a class="nav-link" id="pills-<?php echo $value->slug; ?>-tab" data-toggle="pill" href="#pills-<?php echo $value->slug; ?>" role="tab" aria-controls="pills-home" aria-selected="false">
									  	    	<?php echo $value->slug ?>
									  	    </a>
								  	    </li>
					     <?php else: break;
						    endif; ?>
				  	     <?php } ?>
					</ul>
					<div class="tab-content" id="pills-tabContent">

						<?php $custom_terms = get_terms('client_occupation');

						  	foreach($custom_terms as $key=>$value) {?>
						  		<?php if($key == 0): ?>
									<div class="tab-pane fade-tab show active" id="pills-<?php echo $value->slug; ?>" role="tabpanel" aria-labelledby="pills-<?php echo $value->slug; ?>-tab">
										<div class="container">
											<div class="row">
													<!-- loop start -->
													<?php
													    $args = array(
														'post_type' => 'studio_clients',
														'posts_per_page' => 8,
															'tax_query' => array(
																array(
																	'taxonomy' => 'client_occupation',
																	'field'    => 'slug',
																	'terms'    => $value->slug,
																),
															),
														'no_found_rows' => true, 
														'update_post_meta_cache' => false, 
														'update_post_term_cache' => false, 
														'fields' => 'ids'	
													);
													${"query$key"} = new WP_Query( $args ); ?>

													<?php if ( ${"query$key"}->have_posts() ) : ?>
														<!-- pagination here -->

														<!-- the loop -->
														<?php while ( ${"query$key"}->have_posts() ) : ${"query$key"}->the_post(); ?>
															<div class="col-12 col-sm-6 col-lg-3 p-0">
																<figure class="mx-auto position-relative client_tab_prev">
																	<span class="position-absolute client_tab_prev__go_to_client">
																		<a href="<?php the_permalink(); ?>"><svg class="icon arrow-up-right2"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#arrow-up-right2"></use></svg></a>
																	</span>
																	<div class="client_tab_prev__image">
																		<?php the_post_thumbnail('medium-client-thumnail');?>
																	</div>
																	<h3 class="position-absolute client_tab_prev__heading">
																		<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
																	</h3>
																	<?php $description = get_post_meta( get_the_ID(), 'studio_text', true ); 
																			if($description) : ?>
																			<h6 class="position-absolute client_tab_prev__description">
																			<?php echo $description; ?>
																			</h6>
																	<?php endif; ?>
																</figure>
																<?php  ?>
															</div>
														<?php endwhile; ?>
														<!-- end of the loop -->

														<!-- pagination here -->

														<?php wp_reset_postdata(); ?>

													<?php else : ?>
														<p><?php esc_html_e( 'Sorry, no clients matched your criteria.' ); ?></p>
													<?php endif; ?>
													<!-- loop end -->
												
											</div>
										</div>
									</div>

									<?php elseif($key != 0 && $key < 3): ?>
										<div class="tab-pane fade-tab" id="pills-<?php echo $value->slug; ?>" role="tabpanel" aria-labelledby="pills-<?php echo $value->slug ?>-tab">
											<div class="container">
												<div class="row">
																					
														<!-- loop start -->
														<?php
														    $args = array(
															'post_type' => 'studio_clients',
															'posts_per_page' => 8,
																'tax_query' => array(
																	array(
																		'taxonomy' => 'client_occupation',
																		'field'    => 'slug',
																		'terms'    => $value->slug,
																	),
																),
															'no_found_rows' => true, 
															'update_post_meta_cache' => false, 
															'update_post_term_cache' => false, 
															'fields' => 'ids'	
														);
														${"query$key"} = new WP_Query( $args ); ?>


														<?php if ( ${"query$key"}->have_posts() ) : ?>
															<!-- pagination here -->

															<!-- the loop -->
															<?php while ( ${"query$key"}->have_posts() ) : ${"query$key"}->the_post(); ?>
																<div class="col-12 col-sm-6 col-lg-3 p-0">
																	<figure class="mx-auto position-relative client_tab_prev">
																		<span class="position-absolute client_tab_prev__go_to_client">
																			<a href="<?php the_permalink(); ?>"><svg class="icon arrow-up-right2"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#arrow-up-right2"></use></svg></a>
																		</span>
																		<div class="client_tab_prev__image">
																			<?php the_post_thumbnail('medium-client-thumnail');?>
																		</div>
																		<h3 class="position-absolute client_tab_prev__heading">
																			<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
																		</h3>
																		<?php $description = get_post_meta( get_the_ID(), 'studio_text', true ); 
																			if($description) : ?>
																			<h6 class="position-absolute client_tab_prev__description">
																			<?php echo $description; ?>
																			</h6>
																		<?php endif; ?>
																	</figure>
																	
																</div>
															<?php endwhile; ?>
															<!-- end of the loop -->

															<!-- pagination here -->

															<?php wp_reset_postdata(); ?>

														<?php else : ?>
															<p><?php esc_html_e( 'Sorry, no clients matched your criteria.' ); ?></p>
														<?php endif; ?>
														<!-- loop end -->
												</div>
											</div>
										</div>
						<?php else: break;
						    endif; ?>
						<?php } ?>
					</div>
				</div>
			</div>	
		</div>
		<a href="<?php echo get_page_link( get_page_by_path( 'all-clients' )->ID ); ?>" class="btn btn-lg position-absolute tabs_inner__explore_more" role="button" aria-pressed="true">
			EXPLORE MORE
		</a>
	</section>
	<section class="block">
		<div class="container stories">
			<h1 class="position-relative text-center stories__header">Our Stories</h1>
			<div class="row">
				<?php
					$args = array( 
						'post_type' => 'stories', 
						'posts_per_page' => 3,
						'meta_query' => array( 
					        array(
					            'key' => '_thumbnail_id'
					        ) 
					    ),
					    'no_found_rows' => true, 
						'update_post_meta_cache' => false, 
						'update_post_term_cache' => false
					 );
					$loop = new WP_Query( $args );
					if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();?>
						<div class="col-6 col-xl-4 d-flex justify-content-center justify-content-md-start d-xl-block">
							<div class="d-flex flex-column align-items-center align-items-md-start flex-md-row flex-md-nowrap mb-md-3 mb-xl-0 w-100 stories_block">
								<figure class="mb-3 mb-md-0 story_thumb_cnt">
									<a class="position-relative d-block" href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail('small-thumnail'); ?>	
										<div class="position-absolute d-flex justify-content-center align-items-center overlay">
											<svg class="icon play"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#play"></use></svg>
										</div>
									</a>
								</figure>
								<div class="position-relative align-self-stretch story_info">
									<header class="story_header">
										<h5 class="mb-3">
											<a class="story_link" href="<?php the_permalink(); ?>">
												<?php
											    if (strlen($post->post_title) > 40)
											       echo substr($post->post_title, 0, 40) . ' ...';
											    else
											       echo $post->post_title; ?>

										</a> </h5>
									</header>
									<span class="story_date"><?php the_time('j F, Y'); ?></span>	
								</div>
							</div>
						</div>
	            <?php  
				/* Restore original Post Data */
					wp_reset_postdata();
					endwhile;
					else: ?>
                       <h1>No stories so far(</h1>
				<?php endif; ?>
			</div>
			<div class="row">
				<div class="col-12 col-md-8">
						<div class="row">
							<div class="col-12 featured_story_cnt">
								<div class="featured_story_cnt__inner">
						   			<?php
										$args2 = array( 
											'post_type' => array('stories'),
											'posts_per_page' => -1,
											'meta_query' => array( 
										        array(
										            'key' => '_thumbnail_id'
										        ) 
										    ),
										    'no_found_rows' => true, 
											'update_post_meta_cache' => false, 
											'update_post_term_cache' => false, 
											'fields' => 'ids'

										 );
										$featuredLoop = new WP_Query( $args2 );
										if ($featuredLoop->have_posts()) : while ($featuredLoop->have_posts()) : $featuredLoop->the_post();?>

											<?php $featured = get_post_meta( get_the_ID(), 'studio_featured_1', true );
											    if($featured == 1 && (get_post_format() == 'video')): ?>
											     	<div class="d-flex flex-column-reverse flex-lg-row flex-nowrap">
											     		<div class="d-flex align-items-end featured">
											     			<a class="featured__link" href="<?php the_permalink(); ?>">

											     				<svg class="icon play"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#play"></use></svg>

												     			<h2 class="mb-3">
												     				<?php the_title();?> 
												     			</h2>

												     	        <span class="featured_date">
												     	        	<?php the_time('j F, Y'); ?>
												     	        		
												     	        </span>
												     	    </a>
											     		</div>
											     		<div class="position-relative featured_thumb featured_thumb--index">
											     			<?php the_post_thumbnail('medium-story-thumnail'); ?>
											     			<span class="position-absolute featured_thumb__lable">
											     				trending
											     			</span>
											     		</div>
											     	</div>
		
											    <?php break;
												    endif; ?>												
									<?php  
									/* Restore original Post Data */
										wp_reset_postdata();
										endwhile;
										else: ?>
					                       <h1>No featured stories so far(</h1>
									<?php endif; ?>	
								</div>

							</div>
						</div>
						<div class="row">
							<div class="col-12 py-2 py-md-0">
								<div class="video_slider">
									<div class="owl-carousel">
										
										<?php if (get_theme_mod('video_link_one') !== ''): ?>
											<div class="item-video">
												<a class="owl-video" href="<?php echo get_theme_mod('video_link_one','https://www.youtube.com/watch?v=TblZ0wQ1-B4'); ?>"></a>
											</div>
										<?php endif; ?>

										<?php if (get_theme_mod('video_link_two') !== ''): ?>
											<div class="item-video">
												<a class="owl-video" href="<?php echo get_theme_mod('video_link_two','https://www.youtube.com/watch?v=Bey4XXJAqS8'); ?>"></a>
											</div>
										<?php endif; ?>

										<?php if (get_theme_mod('video_link_three') !== ''): ?>
											<div class="item-video">
												<a class="owl-video" href="<?php echo get_theme_mod('video_link_three','https://www.youtube.com/watch?v=v87mUWsPyeg'); ?>"></a>
											</div>
										<?php endif; ?>

										<?php if (get_theme_mod('video_link_four') !== ''): ?>
											<div class="item-video">
												<a class="owl-video" href="<?php echo get_theme_mod('video_link_four','https://www.youtube.com/watch?v=u3APNJYMrLo') ?>"></a>
											</div>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
				</div>
				<?php  if(is_active_sidebar('bottom_pictures_slider')):?>
				<div class="col-12 col-md-4 portrait_slider_cnt">
					<?php dynamic_sidebar('bottom_pictures_slider'); ?>
				</div>
				<?php endif; ?>
			</div>
		</div>
		
	</section>

<?php		
	get_footer();
?>