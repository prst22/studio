<?php get_header();?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="single_post_page">
                    <?php while ( have_posts() ) : the_post(); ?>
                        <div class="main_post clearfix">

                            <?php if ( is_singular( 'stories' ) ) : ?>
                                <?php $id = get_the_ID(); ?>
                                <?php get_template_part( 'content', 'single_news_stories' ); ?>

                            <?php elseif (is_singular( 'studio_clients' )):  ?>
                                <?php $id = get_the_ID(); ?>

                                <?php  
                                    $customTaxonomy = get_the_terms( $post->ID , 'client_occupation' ); 
                                    $outputForSmallScreens = '';
                                    if($customTaxonomy){
                                        foreach ($customTaxonomy as $taxForSmallScreens) {
                                            $outputForSmallScreens .= '<span>' .$taxForSmallScreens->name . ' |</span>';
                                        }
                                    }
                                    $post = get_queried_object();
                                    $postTypeName = get_post_type_object(get_post_type($post));
                                ?>

                                <div class="position-relative main_post__heading">

                                    <h1 class="heading_title"><?php the_title(); ?></h1>
                                    <div class="heading_info">
                                        <span class="post_type">
                                            <b>
                                                <?php if($postTypeName){echo esc_html($postTypeName->labels->name).'|';} ?>    
                                            </b>
                                        </span> 
                                        <?php echo $outputForSmallScreens; ?> 
                                        <span><?php the_time('F j, Y'); ?></span>
                                    </div>
                                        <div class="position-absolute d-xl-flex flex-column align-items-end heading_info_md">
                                            <span class="post_type">
                                                <b>
                                                <?php if($postTypeName){echo esc_html($postTypeName->labels->name);} ?>
                                                </b>
                                            </span>
                                            <?php
                                                $outputForLargeScreens = '';
                                                if($customTaxonomy){
                                                    foreach($customTaxonomy as $tax){
                                                        $outputForLargeScreens .= '<span><b>' . $tax->name . '</b></span>';
                                                    }
                                                    echo $outputForLargeScreens;
                                                }
                                            ?>
                                        
                                        <span>
                                            <?php the_time('F j, Y'); ?>
                                        </span>
                                       
                                    </div>
                                </div>
                                <?php if(has_post_thumbnail()): ?>
                                <div class="float-left w-100  post_main_client_img_outer">
                                    <div class="post_main_client_img">
                                        <?php the_post_thumbnail('medium-client-thumnail', 
                                            $attr = array(
                                                'alt'   => "post client image",
                                                'class' => "post_main_client_img__client_image"
                                            )); ?>
                                    </div>
                                </div>
                                <?php  the_content(); ?>

                                <?php endif; ?>

                            <?php else: ?>
                                <?php $id = get_the_ID(); ?>
                                <?php get_template_part( 'content', 'single_news_stories' ); ?>

                            <?php endif;  ?>
                        </div>
                    <?php endwhile; ?>

                </div>
            </div>
        </div>
    </div>

    <?php if((is_singular( 'stories' ) || is_singular( 'post' ))): ?>

    <?php 
        $category = get_the_category();
        $firstCategory = $category[0]->slug; 
        $currentPostType = get_post_type();
    ?>

    <?php if(isset($category) && isset($firstCategory) && isset($firstCategory)): ?>

        <div class="container-fluid">
            <div class="row">
                <div class="col-12 px-0">
                    <h1 class="position-relative text-center news__header">You may also like</h1>
                    <div class="single_post_page_slider">
                        <div class="owl-carousel">

                            <?php $args = array( 
                                'post_type' => $currentPostType,
                                'posts_per_page' => 8,
                                'category_name'  => $firstCategory,
                                'post__not_in' => array($id),
                                'meta_query' => array( 
                                    array(
                                        'key' => '_thumbnail_id'
                                    ) 
                                ),
                                'no_found_rows' => true, 
                                'update_post_meta_cache' => false, 
                                'update_post_term_cache' => false
                             );
                                $singlePostSliderLoop = new WP_Query( $args );
                                if ($singlePostSliderLoop->have_posts()) : while ($singlePostSliderLoop->have_posts()) : $singlePostSliderLoop->the_post();?>
                                    <div class=" col-12 w-100 p-0 grid_item">
                                        <figure class="mb-0 single_post_page_slider_thumb">
                                            <a href="<?php the_permalink(); ?>">
                                                <?php the_post_thumbnail('small-thumnail', 
                                                    $attr = array(
                                                        'class' => "owl-lazy",
                                                        'alt'   => "slider image",
                                                        'data-src' => get_the_post_thumbnail_url( get_the_ID(), 'small-thumnail')
                                                    )); ?>
                                            </a>
                                        </figure>
                                            
                                        <h5 class="position-relative px-2 px-sm-3 mb-0 grid_item__heading">
                                            <a href="<?php the_permalink(); ?>">
                                            <?php
                                                if (strlen($post->post_title) > 30)
                                                   echo substr($post->post_title, 0, 30) . ' ...';
                                                else
                                                   echo $post->post_title; 
                                               ?>

                                            </a>
                                            <span class="news_stories_date"><?php the_time('j F, Y'); ?></span>
                                        </h5>
                                    </div> 
                            <?php  
                            /* Restore original Post Data */
                                wp_reset_postdata();
                                endwhile;
                                else: ?>
                                   <h1>No clients so far(</h1>
                            <?php endif;?>
                            
                        </div>
                        <div class="text-center d-flex align-items-center justify-content-center single_post_page_slider__dots"></div>   
                    </div>
                </div>

            <?php endif; ?>   
        </div>
        <!-- start of studio clients bottom related posts -->
        <?php elseif(is_singular( 'studio_clients' )): ?>
            
            <?php 

            $custom_terms = get_the_terms( get_the_ID(), 'client_occupation');

                    if(isset($custom_terms) && isset($custom_terms[0]->slug)):?>

                    <div class="container position-relative mx-auto tabs_inner">
                        <div class="row related_single_post_clients">
                            <div class="col-12 px-0">
                                <h1 class="position-relative text-center news__header">You may also like</h1>
                            </div>

                        <?php $args2 = array( 
                            'post_type' => 'studio_clients',
                            'posts_per_page' => 4,
                            'post__not_in' => array($id),
                            'meta_query' => array( 
                                array(
                                    'key' => '_thumbnail_id'
                                ) 
                            ),
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'client_occupation',
                                    'field'    => 'slug',
                                    'terms'    => $custom_terms[0]->slug,
                                ),
                            ),
                            'no_found_rows' => true, 
                            'update_post_meta_cache' => false, 
                            'update_post_term_cache' => false
                        );

                        $singeleClientsLoop = new WP_Query( $args2 ); 
                        if ($singeleClientsLoop->have_posts()) : while ($singeleClientsLoop->have_posts()) : $singeleClientsLoop->the_post();?>

                            <div class="col-12 col-sm-6 col-lg-3 p-0">
                                <figure class="mx-auto position-relative client_tab_prev">
                                    <span class="position-absolute client_tab_prev__go_to_client">
                                        <a href="<?php the_permalink(); ?>"><svg class="icon arrow-up-right2"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#arrow-up-right2"></use></svg></a>
                                    </span>
                                    <div class="client_tab_prev__image">
                                        <?php the_post_thumbnail('medium-client-thumnail');?>
                                    </div>
                                    <h3 class="position-absolute client_tab_prev__heading">
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </h3>
                                    <?php $description = rwmb_meta( 'studio_text' ); 
                                            if($description) : ?>
                                            <h6 class="position-absolute client_tab_prev__description">
                                            <?php echo $description; ?>
                                            </h6>
                                    <?php endif; ?>
                                </figure>
                                <?php  ?>
                            </div>

                        <?php  
                        /* Restore original Post Data */
                            wp_reset_postdata();
                            endwhile;
                            else: ?>

                               <h1>No clients so far(</h1>

                        <?php endif;?> 

                <?php endif; ?>
                <!-- custom term check end -->
               </div>
           </div>

    <?php endif; ?><!-- end of studio clients bottom related posts -->
</section>
<?php get_footer();?>