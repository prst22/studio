<?php	
/*
	
@package sunsettheme
	
	========================
		WIDGET CLASS
	========================
*/
class Studio_Bottom_Widget extends WP_Widget {
	
	// setup the widget name, description, etc...
	public function __construct() {
		
		$widget_ops = array(
			'classname' => 'studio_bottom',
			'description' => 'Custom Slider Widget',
		);
		parent::__construct( 'studio_slider', 'Studio Slider', $widget_ops );
		
	}
	
	// back-end display of widget
	public function form( $instance ) {

		$slideImageOne = ! empty( $instance['slideimageone'] ) ? $instance['slideimageone'] : '';
		$slideTitleOne = ! empty( $instance['slidetitleone'] ) ? $instance['slidetitleone'] : esc_html__( 'New title', 'studio' );

		$slideImageTwo = ! empty( $instance['slideimagetwo'] ) ? $instance['slideimagetwo'] : '';
		$slideTitleTwo = ! empty( $instance['slidetitletwo'] ) ? $instance['slidetitletwo'] : esc_html__( 'New title', 'studio' ); 

		$slideImageThree = ! empty( $instance['slideimagethree'] ) ? $instance['slideimagethree'] : '';
		$slideTitleThree = ! empty( $instance['slidetitlethree'] ) ? $instance['slidetitlethree'] : esc_html__( 'New title', 'studio' );

		echo '<p><strong>Incert a 520px by 845px picture</strong>';
		?>

        <p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'slideimageone' ) ); ?>"><?php esc_attr_e( 'Image one:', 'studio' ); ?></label> 
			<input class="widefat image-upload" id="<?php echo esc_attr( $this->get_field_id( 'slideimageone' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'slideimageone' ) ); ?>" type="text" value="<?php echo esc_url( $slideImageOne ); ?>">
			<button type="button" class="button button-primary js-image-upload">Select Image</button>
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'slidetitleone' ) ); ?>"><?php esc_attr_e( 'Slide one title:', 'studio' ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'slidetitleone' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'slidetitleone' ) ); ?>" type="text" value="<?php echo esc_attr( $slideTitleOne ); ?>">
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'slideimagetwo' ) ); ?>"><?php esc_attr_e( 'Image two:', 'studio' ); ?></label> 
			<input class="widefat image-upload" id="<?php echo esc_attr( $this->get_field_id( 'slideimagetwo' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'slideimagetwo' ) ); ?>" type="text" value="<?php echo esc_url( $slideImageTwo ); ?>">
			<button type="button" class="button button-primary js-image-upload">Select Image</button>
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'slidetitletwo' ) ); ?>"><?php esc_attr_e( 'Slide two title:', 'studio' ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'slidetitletwo' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'slidetitletwo' ) ); ?>" type="text" value="<?php echo esc_attr( $slideTitleTwo ); ?>">
		</p>
		
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'slideimagethree' ) ); ?>"><?php esc_attr_e( 'Image three:', 'studio' ); ?></label> 
			<input class="widefat image-upload" id="<?php echo esc_attr( $this->get_field_id( 'slideimagethree' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'slideimagethree' ) ); ?>" type="text" value="<?php echo esc_url( $slideImageThree ); ?>">
			<button type="button" class="button button-primary js-image-upload">Select Image</button>
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'slidetitlethree' ) ); ?>"><?php esc_attr_e( 'Slide three title:', 'studio' ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'slidetitlethree' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'slidetitlethree' ) ); ?>" type="text" value="<?php echo esc_attr( $slideTitleThree ); ?>">
		</p>

        <?php
	}
	
	//front-end display of widget
	public function widget( $args, $instance ) {
		
		echo $args['before_widget'];
		?>
		<div class="owl-carousel">
			<?php if(! empty( $instance['slideimageone'] )): ?>

				<div>
					<div class="portrait_image_cnt">
						<img class="owl-lazy portrait_image_cnt__image"

                        <?php 
	                        if(! empty( $instance['slidetitleone'] )){
	                        	echo 'alt="'.  esc_attr($instance['slidetitleone']) .'"';
	                        } 
                        ?>
						
						data-src="<?php echo esc_url($instance['slideimageone']); ?>">
					</div>
                    <?php if(! empty( $instance['slidetitleone'] )): ?>

					<h5 class="portrait_slider_header">
						<?php echo esc_attr($instance['slidetitleone']); ?>
					</h5>

				    <?php endif; ?>
				</div>

			<?php endif; ?>

			<?php if(! empty( $instance['slideimagetwo'] )): ?>

				<div>
					<div class="portrait_image_cnt">
						<img class="owl-lazy portrait_image_cnt__image"

                        <?php 
	                        if(! empty( $instance['slidetitletwo'] )){
	                        	echo 'alt="'.  esc_attr($instance['slidetitletwo']) .'"';
	                        } 
                        ?>
						
						data-src="<?php echo esc_url($instance['slideimagetwo']); ?>">
					</div>
                    <?php if(! empty( $instance['slidetitletwo'] )): ?>

					<h5 class="portrait_slider_header">
						<?php echo esc_attr($instance['slidetitletwo']); ?>
					</h5>

				    <?php endif; ?>
				</div>

			<?php endif; ?>

			
			<?php if(! empty( $instance['slideimagethree'] )): ?>

				<div>
					<div class="portrait_image_cnt">
						<img class="owl-lazy portrait_image_cnt__image"

                        <?php 
	                        if(! empty( $instance['slidetitlethree'] )){
	                        	echo 'alt="'.  esc_attr($instance['slidetitlethree']) .'"';
	                        } 
                        ?>
						
						data-src="<?php echo esc_url($instance['slideimagethree']); ?>">
					</div>
                    <?php if(! empty( $instance['slidetitlethree'] )): ?>

					<h5 class="portrait_slider_header">
						<?php echo esc_attr($instance['slidetitlethree']); ?>
					</h5>

				    <?php endif; ?>
				</div>

			<?php endif; ?>

		</div>
		<?php
		echo $args['after_widget'];
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();

		$instance['slideimageone'] = ! empty( $new_instance['slideimageone'] ) ? $new_instance['slideimageone'] : '';
		$instance['slidetitleone'] = ( ! empty( $new_instance['slidetitleone'] ) ) ? sanitize_text_field( $new_instance['slidetitleone'] ) : '';

		$instance['slideimagetwo'] = ! empty( $new_instance['slideimagetwo'] ) ? $new_instance['slideimagetwo'] : '';
		$instance['slidetitletwo'] = ( ! empty( $new_instance['slidetitletwo'] ) ) ? sanitize_text_field( $new_instance['slidetitletwo'] ) : '';

		$instance['slideimagethree'] = ! empty( $new_instance['slideimagethree'] ) ? $new_instance['slideimagethree'] : '';
		$instance['slidetitlethree'] = ( ! empty( $new_instance['slidetitlethree'] ) ) ? sanitize_text_field( $new_instance['slidetitlethree'] ) : '';

		return $instance;
	}
}
add_action( 'widgets_init', function() {
	register_widget( 'Studio_Bottom_Widget' );
} );

