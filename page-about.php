<?php
/* Template Name: About Page */
?>
<?php get_header();?>
<section class="block about_bg">
	<div class="d-flex flex-row flex-wrap flex-md-nowrap">
		<div class="position-relative about_contact">
			<img src="<?php echo get_template_directory_uri(); ?>/images/1.jpg" alt="woman">
			<div class="position-absolute d-flex flex-row flex-nowrap about_contact__btn_cnt">
				<button type="button" class="d-flex align-items-center justify-content-center contact_btn" id="about_contact_btn" data-toggle="modal" data-target="#contactModalCenter">contact us</button>
				<!-- Modal -->
				<div class="modal fade" id="contactModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLongTitle"><b>Contact info</b></h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">
				          	<svg class="icon close-cross"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#close-cross"></use></svg>
				          </span>
				        </button>
				      </div>
				      <div class="modal-body">
			    		<ul class="pl-4">
							<li>
			    				<span>Country:</span>
							    Netherlands
							</li>
							<li>
			    				<span>City:</span>
							    Utrecht
							</li>
							<li>
			    				<span>Street:</span>
							    Armin van buuren 44/5 
							</li>
							<li>
			    				<span>E-mail:</span>
							    <a href="mailto:admin@potatosites.com" target="_blank">
								    admin@potatosites.com
								</a>
							</li>
							<li>
			    				<span>Tel:</span>
							    +44 44 444 4444
							</li>
			    		</ul>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				      </div>
				    </div>
				  </div>
				</div>
				<div class="d-flex align-items-center justify-content-center contact_lable">
					<svg class="icon user"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#user"></use></svg>
				</div>
			</div>
		</div>	
		<div class="container about">
			<div class="row">
				<div class="col-12"> 
					<div class="mx-auto about__top">
						<h1 class="about_main_heading"><?php the_title();?></h1>
						
							<?php if ( have_posts() ) : ?>
								<?php while ( have_posts() ) : the_post(); ?>  
								<p>  
								<?php the_content(); ?>
								</p>
								<?php endwhile; ?>
							<?php endif; ?>
										
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="pt-0 block block--tabs_cnt">
	<div class="container pb-5 position-relative mx-auto tabs_inner">
		<div class="row">
			<div class="col-12">
				<ul class="nav d-flex justify-content-center tabs_controlls" id="pills-tab" role="tablist">
					<?php $custom_terms = get_terms('client_occupation');
					  	    foreach($custom_terms as $key=>$value) {?>
					  	    	<?php if($key == 0): ?>
						  	    	<li class="nav-item text-uppercase">
						  	    		<a class="nav-link active" id="pills-<?php echo $value->slug; ?>-tab" data-toggle="pill" href="#pills-<?php echo $value->slug; ?>" role="tab" aria-controls="pills-home" aria-selected="true">
								  	    	<?php echo $value->slug ?>
								  	    </a>
							  	    </li>
							  	<?php  
							    elseif($key < 3): ?>
							    	<li class="nav-item text-uppercase">
						  	    		<a class="nav-link" id="pills-<?php echo $value->slug; ?>-tab" data-toggle="pill" href="#pills-<?php echo $value->slug; ?>" role="tab" aria-controls="pills-home" aria-selected="false">
								  	    	<?php echo $value->slug ?>
								  	    </a>
							  	    </li>
				     <?php else: break;
					    endif; ?>
			  	     <?php } ?>
				</ul>
				<div class="tab-content" id="pills-tabContent">

					<?php $custom_terms = get_terms('client_occupation');

					  	foreach($custom_terms as $key=>$value) {?>
					  		<?php if($key == 0): ?>
								<div class="tab-pane fade-tab show active" id="pills-<?php echo $value->slug; ?>" role="tabpanel" aria-labelledby="pills-<?php echo $value->slug; ?>-tab">
									<div class="container">
										<div class="row carousel_cnt <?php print "carousel".$key; ?>">
												<!-- loop start -->
												<?php
												    $args = array(
													'post_type' => 'studio_clients',
													'posts_per_page' => 8,
														'tax_query' => array(
															array(
																'taxonomy' => 'client_occupation',
																'field'    => 'slug',
																'terms'    => $value->slug,
															),
														),
													'no_found_rows' => true, 
													'update_post_meta_cache' => false, 
													'update_post_term_cache' => false, 
													'fields' => 'ids'	
												);
												${"query$key"} = new WP_Query( $args ); ?>

												<?php if ( ${"query$key"}->have_posts() ) : ?>

													<div class="owl-carousel">
													

													<!-- the loop -->
													<?php while ( ${"query$key"}->have_posts() ) : ${"query$key"}->the_post(); ?>
														
															<figure class="mx-auto position-relative client_tab_prev">
																<span class="position-absolute client_tab_prev__go_to_client">
																	<a href="<?php the_permalink(); ?>"><svg class="icon arrow-up-right2"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#arrow-up-right2"></use></svg></a>
																</span>
																<div class="client_tab_prev__image">
																	<?php the_post_thumbnail('medium-client-thumnail');?>
																</div>
																<h3 class="position-absolute client_tab_prev__heading">
																	<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
																</h3>
																<?php $description = get_post_meta( get_the_ID(), 'studio_text', true );
																		if($description) : ?>
																		<h6 class="position-absolute client_tab_prev__description">
																		<?php echo $description; ?>
																		</h6>
																<?php endif; ?>
															</figure>
															<?php  ?>
														
													<?php endwhile; ?>
													<!-- end of the loop -->

													<!-- pagination here -->
													</div>

													<?php wp_reset_postdata(); ?>

												<?php else : ?>
													<p><?php esc_html_e( 'Sorry, no clients matched your criteria.' ); ?></p>
												<?php endif; ?>
												<!-- loop end -->
											
										</div>
									</div>
								</div>

								<?php elseif($key != 0 && $key < 3): ?>
									<div class="tab-pane fade-tab" id="pills-<?php echo $value->slug; ?>" role="tabpanel" aria-labelledby="pills-<?php echo $value->slug ?>-tab">
										<div class="container">
											<div class="row carousel_cnt <?php print "carousel".$key; ?>">
																				
													<!-- loop start -->
													<?php
													    $args = array(
														'post_type' => 'studio_clients',
														'posts_per_page' => 8,
															'tax_query' => array(
																array(
																	'taxonomy' => 'client_occupation',
																	'field'    => 'slug',
																	'terms'    => $value->slug,
																),
															),
														'no_found_rows' => true, 
														'update_post_meta_cache' => false, 
														'update_post_term_cache' => false, 
														'fields' => 'ids'
													);
													${"query$key"} = new WP_Query( $args ); ?>

													<?php if ( ${"query$key"}->have_posts() ) : ?>
														<div class="owl-carousel">

														<!-- the loop -->
														<?php while ( ${"query$key"}->have_posts() ) : ${"query$key"}->the_post(); ?>
															
																<figure class="mx-auto position-relative client_tab_prev">
																	<span class="position-absolute client_tab_prev__go_to_client">
																		<a href="<?php the_permalink(); ?>"><svg class="icon arrow-up-right2"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#arrow-up-right2"></use></svg></a>
																	</span>
																	<div class="client_tab_prev__image">
																		<?php the_post_thumbnail('medium-client-thumnail');?>
																	</div>
																	<h3 class="position-absolute client_tab_prev__heading">
																		<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
																	</h3>
																	<?php $description = get_post_meta( get_the_ID(), 'studio_text', true ); 
																		if($description) : ?>
																		<h6 class="position-absolute client_tab_prev__description">
																		<?php echo $description; ?>
																		</h6>
																	<?php endif; ?>
																</figure>
																
															
														<?php endwhile; ?>
														<!-- end of the loop -->
														</div>

														<!-- pagination here -->

														<?php wp_reset_postdata(); ?>

													<?php else : ?>
														<p><?php esc_html_e( 'Sorry, no clients matched your criteria.' ); ?></p>
													<?php endif; ?>
													<!-- loop end -->
											</div>
										</div>
									</div>
					<?php else: break;
					    endif; ?>
					<?php } ?>
				</div>
			</div>
		</div>	
	</div>
</section>

<?php get_footer();?>