<?php
function my_admin_scripts(){
    wp_enqueue_media();
    wp_enqueue_script('media-upload');
    wp_enqueue_script('my-script', get_template_directory_uri() . '/admin/my_script.js', array('jquery'), '', true);
}
add_action('admin_enqueue_scripts', 'my_admin_scripts');