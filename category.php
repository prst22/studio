<?php
	get_header();
?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="container single_page">
				<?php
				   
				    $queried_object = get_queried_object();
				    ?>
					
					<h1 class="mb-5 seach_cat_main_heading"><?php echo 'Category: ' . $queried_object->name; ?> </h1>
					<?php
					
					if (have_posts()) : while (have_posts()) : the_post();?>
						<?php if ( get_post_type( get_the_ID() ) !== 'stories' ): ?>
							<div class="row">
								<div class="col-12">

									<h4 class="mb-0">
										<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									</h4>
									  <?php $postType = get_post_type_object(get_post_type());
									    if ($postType) :?>
									    <span class="serch_cat_info">
									    	<a href="<?php echo get_page_link( get_page_by_path( 'all-news' )->ID ); ?>">	
												<?php echo esc_html($postType->labels->singular_name) . ', '; ?>
										    </a>
										    <?php endif; ?>												     	        	
											<?php the_time('j F, Y'); ?>
										</span>
								</div>

								<div class="col-12 mb-3 mb-md-4">

								    <figure class="float-left mb-0 mb-lg-2 mr-2 cat_serch_image_cnt">
										<a href="<?php the_permalink() ?>" class="d-block position-relative cat_serch_image_cnt__link_cnt">
											<?php the_post_thumbnail('small-thumnail',
												$attr = array('alt'   => "category image"));?>
										</a>
									</figure>	

								    <p class="serch_cat_exc"><?php the_excerpt_rss(); ?></p>

							    </div>
						    </div>
						    <?php elseif( get_post_type( get_the_ID() ) == 'stories' ): ?>
							    <div class="row">
							    	<div class="col-12 mb-3 mb-md-4">

									    <figure class="float-left mb-0 mb-lg-2 mr-2 cat_serch_image_cnt">
											<a href="<?php the_permalink() ?>" class="d-block position-relative cat_serch_image_cnt__link_cnt">
												<?php the_post_thumbnail('small-thumnail',
													$attr = array('alt'   => "category image"));?>
												<div class="position-absolute d-flex justify-content-center align-items-center overlay">
													<svg class="icon play"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#play"></use></svg>
												</div>

											</a>
										</figure>

										<?php $postType = get_post_type_object(get_post_type());
									    if ($postType) :?>
									    <span class="serch_cat_info">
									    	<a href="<?php echo get_page_link( get_page_by_path( 'all-stories' )->ID ); ?>">	
												<?php echo esc_html($postType->labels->singular_name) . ', '; ?>
										    </a>
										    <?php endif; ?>												     	        	
											<?php the_time('j F, Y'); ?>
										</span>	
									    <h5 class="serch_cat_heading"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
									   
								    </div>
							    </div>

						<?php endif ?>
						
					<?php /* Restore original Post Data */
							wp_reset_postdata(); ?>
					<?php endwhile; ?>
					<?php endif; ?>

					<?php if(is_paginated()): ?>										
						<div class="row">
							<div class="col-12 py-3">
								<div class="pagination_cnt">
									<div class="d-flex justify-content-between pagination_cnt__inner">
										<?php previous_posts_link( '<span class="iconslider icon-arrow-left2 px-1"></span>prev page '); ?>
								        <?php next_posts_link( 'next page <span class="iconslider icon-arrow-right2 px-1"></span>'); ?>
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php		
	get_footer();
?>