let j = jQuery.noConflict();

let font1 = new FontFaceObserver('Nunito', {
          weight: 400
    });
let font2 = new FontFaceObserver('Nunito', {
          weight: 600
    });
let font3 = new FontFaceObserver('Nunito', {
      weight: 700
});
let font4 = new FontFaceObserver('Nunito', {
      weight: 900
});
let font5 = new FontFaceObserver('Poppins', {
      weight: 500
});
let font6 = new FontFaceObserver('Poppins', {
      weight: 400
});
let html = document.documentElement;

html.classList.add('fonts-loading');
if(!sessionStorage.fontsLoaded){
    Promise.all([font1.load(null, 12000), font2.load(null, 12000), font3.load(null, 12000), font4.load(null, 12000), font5.load(null, 12000), font6.load(null, 12000)]).then(function () {
        html.classList.remove('fonts-loading');
        html.classList.add('fonts-loaded');
        sessionStorage.fontsLoaded = true;
    }).catch(function () {
        html.classList.remove('fonts-loading');
        html.classList.add('fonts-failed');
        sessionStorage.fontsLoaded = false;
    });
}else{
    html.classList.remove('fonts-loading');
    html.classList.add('fonts-loaded');
    sessionStorage.fontsLoaded = true;
}
if(j('body').is('.home')){
    j('.index_top_slider>.owl-carousel').owlCarousel({
        lazyLoad: true,
        lazyLoadEager: 1,
        checkVisible: false,
        items: 1,
        rewind: true,
        autoplay: true,
        autoplayTimeout: 5000,
        animateIn: 'fadeInUp',
        animateOut: 'fadeOutDown',
        slideTransition:'ease-in-out',
        responsiveRefreshRate: 50,
        mouseDrag: false,
        pullDrag: false,
        touchDrag: false
    });
    j('.owl-dot').each(function(index){
        let slideNum = j('.owl-dot').length - index;
        this.innerHTML = "0" + slideNum;
    });
    j('.slider_number').each(function(index){
        let slideNum = j('.slider_number').length - index;
        this.innerHTML = "0" + slideNum; 
        // }
    });
    if(j('.video_slider>.owl-carousel')){
        j('.video_slider>.owl-carousel').owlCarousel({
            items: 1,
            loop: false,
            video: true,
            animateIn: 'fadeInUp',
            animateOut: 'fadeOutDown',
            slideTransition:'ease-in-out',
            responsiveRefreshRate: 100
        });
    }
    if(j('.portrait_slider_cnt__inner>.owl-carousel')){
        j('.portrait_slider_cnt__inner>.owl-carousel').owlCarousel({
            lazyLoad: true,
            lazyLoadEager: 1,
            items: 1,
            slideTransition: 'ease-in-out',
            responsiveRefreshRate: 50,
            autoplayHoverPause: true,
            autoplay: true,
            autoplayTimeout: 4000,
            rewind: true,
            autoHeight: true
        });
    }
}
if(j('body').is('.page-template-page-all-clients')){
    j('.index_top_slider--clients>.owl-carousel').owlCarousel({
        lazyLoad: true,
        lazyLoadEager: 1,
        nav: true,
        dots: false,
        checkVisible: false,
        items: 1,
        rewind: true,
        autoplay: false,
        animateIn: 'fadeInUp',
        animateOut: 'fadeOutDown',
        slideTransition:'ease-in-out',
        responsiveRefreshRate: 50,
        mouseDrag: false,
        pullDrag: false,
        touchDrag: false,
        navContainer: '.slide_description_block__owl_nav_cnt',
        navText:['<span class="iconslider icon-arrow-up2"></span>','<span class="iconslider icon-arrow-down2"></span>']
    });
    j('.slider_number').each(function(index){
        let slideNum = j('.slider_number').length - index;
        this.innerHTML = "0" + slideNum; 
        // }
    });
}
if(j('body').is('.page_about')){
    j('.row>.owl-carousel').owlCarousel({
        items: 4,
        loop: false,
        responsiveRefreshRate: 50,
        nav: true,
        dots: false,
        slideTransition:'ease-in-out',
        navText:['<span class="iconslider icon-arrow-left2"></span>','<span class="iconslider icon-arrow-right2"></span>'],
        responsive: {
        // breakpoint from 0 up
            0: {
                items:1
            },
            // breakpoint from 480 up
            480: {
                items:1
            },
            // breakpoint from 768 up
            768: {
                items:2
            },
            992: {
                items:4
            }
        }
    });
}
//news_stories slider start
if(j('body').is('.all_news_page')){
    j('.news_stories_slider>.owl-carousel').owlCarousel({
        items: 1,
        loop: false,
        lazyLoad: true,
        lazyLoadEager: 1,
        autoplay: true,
        rewind: true,
        autoplayTimeout: 4000,
        autoplayHoverPause: true,
        responsiveRefreshRate: 50,
        dots: false,
        nav: true,
        slideTransition:'ease-in-out',
        mouseDrag: false,
        pullDrag: false,
        touchDrag: false,
        navContainer: '.news_stories_slider_nav_cnt__inner',
        navText:['<span class="iconslider icon-arrow-left2"></span>','<span class="iconslider icon-arrow-right2"></span>'],
    });
    j('.carousel_cnt_all_news>.owl-carousel').owlCarousel({
        items: 4,
        loop: false,
        responsiveRefreshRate: 50,
        nav: true,
        dots: false,
        slideTransition:'ease-in-out',
        navText:['<span class="iconslider icon-arrow-left2"></span>','<span class="iconslider icon-arrow-right2"></span>'],
        responsive: {
        // breakpoint from 0 up
            0: {
                items:1
            },
            // breakpoint from 480 up
            480: {
                items:1
            },
            // breakpoint from 768 up
            768: {
                items:2
            },
            992: {
                items:4
            }
        }
    });
}
if(j('body').is('.all_stories_page')){
    j('.news_stories_slider>.owl-carousel').owlCarousel({
        items: 1,
        lazyLoad: true,
        lazyLoadEager: 1,
        loop: false,
        rewind: true,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplayHoverPause: true,
        responsiveRefreshRate: 50,
        dots: false,
        nav: true,
        slideTransition:'ease-in-out',
        mouseDrag: false,
        pullDrag: false,
        touchDrag: false,
        navContainer: '.news_stories_slider_nav_cnt__inner',
        navText:['<span class="iconslider icon-arrow-left2"></span>','<span class="iconslider icon-arrow-right2"></span>'],
    });
    j('.carousel_cnt_all_news>.owl-carousel').owlCarousel({
        items: 4,
        loop: false,
        responsiveRefreshRate: 50,
        slideTransition:'ease-in-out',
        nav: true,
        dots: false,
        navText:['<span class="iconslider icon-arrow-left2"></span>','<span class="iconslider icon-arrow-right2"></span>'],
        responsive: {
        // breakpoint from 0 up
            0: {
                items:1
            },
            // breakpoint from 480 up
            480: {
                items:1
            },
            // breakpoint from 768 up
            768: {
                items:2
            },
            992: {
                items:4
            }
        }
    });
}
if(j('body').is('.single')){
    j('.single_post_page_slider>.owl-carousel').owlCarousel({
        items: 4,
        loop: true,
        responsiveRefreshRate: 50,
        lazyLoad: true,
        lazyLoadEager: 1,
        nav: false,
        dots: true,
        slideTransition:'ease-in-out',
        dotsContainer:'.single_post_page_slider__dots',
        responsive: {
        // breakpoint from 0 up
            0: {
                items:1
            },
            // breakpoint from 768 up
            768: {
                items:2
            },
            992: {
                items:3
            },
            1650: {
                stagePadding: 150
            }
        }
    });
}
//news_stories slider end
function openMenu(){
    j('.main_menu').css('animation-duration', "0.7s");
    j('.main_menu').removeClass('fadeOutLeft');
    j('.main_menu').addClass('fadeInLeft');
    j('#close_menu').css('animation-duration', "0.7s");
    j('#close_menu').removeClass('bounceOut');
    j('#close_menu').addClass('bounceIn');
}
function closeMenu(){
    j('.main_menu').removeClass('fadeInLeft');
    j('.main_menu').addClass('fadeOutLeft');
    j('#close_menu').removeClass('bounceIn');
    j('#close_menu').addClass('bounceOut');
}
if(j('.main_menu')) document.onkeydown = function(evt) {
    evt = evt || window.event;
    var isEscape = false;
    if("key" in evt){
        isEscape = (evt.key == "Escape" || evt.key == "Esc");
    }else{
        isEscape = (evt.keyCode == 27);
    }
    if(isEscape){
        closeMenu();
    }
}
j('#menu_button').click(openMenu);
j('#close_menu').click(closeMenu);
