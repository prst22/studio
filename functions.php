<?php

	//include walker class start

	require get_template_directory() . '/inc/customizer.php';
	require get_template_directory() . '/inc/widget.php';
	require get_template_directory() . '/admin-functions.php';
    
    //include walker class end

	//add theme support
	function add_q() {
		// Adding menus to wp controll pannel
		register_nav_menus(array(
			'main_menu' =>__('Main menu')
		));

	    //post thum pictures
	    add_theme_support('post-thumbnails');
	    add_image_size('small-thumnail', 200, 140, array('center','center'));
	    add_image_size('medium-story-thumnail', 683, 520, array('center','center'));
	    add_image_size('medium-client-thumnail', 420, 490, array('center','center'));  
	    add_image_size('single-story-thumnail', 1008, 567, array('center','center'));  
	    add_image_size('large-client-thumnail', 1122, 810, array('center','center'));
	   
	    add_theme_support( 'post-formats', array( 'video' ));
   
	    //CUSTOM WORDPRESS EDITOR STYLE
		add_editor_style( 'css/custom-editor-styles.min.css' );
	    //CUSTOM WORDPRESS EDITOR STYLE 
	    
	}
	add_action('after_setup_theme', 'add_q');

    // rename post into news start
	function revcon_change_post_label() {
	    global $menu;
	    global $submenu;
	    $menu[5][0] = 'News';
	    $submenu['edit.php'][5][0] = 'News';
	    $submenu['edit.php'][10][0] = 'Add News';
	    $submenu['edit.php'][16][0] = 'News Tags';
	}
	function revcon_change_post_object() {
	    global $wp_post_types;
	    $labels = &$wp_post_types['post']->labels;
	    $labels->name = 'News';
	    $labels->singular_name = 'News';
	    $labels->add_new = 'Add News';
	    $labels->add_new_item = 'Add News';
	    $labels->edit_item = 'Edit News';
	    $labels->new_item = 'News';
	    $labels->view_item = 'View News';
	    $labels->search_items = 'Search News';
	    $labels->not_found = 'No News found';
	    $labels->not_found_in_trash = 'No News found in Trash';
	    $labels->all_items = 'All News';
	    $labels->menu_name = 'News';
	    $labels->name_admin_bar = 'News';
	}
	 
	add_action( 'admin_menu', 'revcon_change_post_label' );
	add_action( 'init', 'revcon_change_post_object' );

    // rename post into news end

    // registering new post type clients start


    // register custom taxonomy for clients post type start 

	// hook into the init action and call create_clients_occupations_taxonomies when it fires
	add_action( 'init', 'clients_occupations', 0 );

	// create two taxonomies, genres and writers for the post type "book"
	function clients_occupations() {
		// Add new taxonomy, make it hierarchical (like categories)
		$labels = array(
			'name'              => _x( 'Clients Occupations', 'taxonomy general name', 'studio' ),
			'singular_name'     => _x( 'Client Occupation', 'taxonomy singular name', 'studio' ),
			'search_items'      => __( 'Search For Client Occupation', 'studio' ),
			'all_items'         => __( 'All Clients Occupations', 'studio' ),
			'parent_item'       => __( 'Parent Occupation', 'studio' ),
			'parent_item_colon' => __( 'Parent Occupation:', 'studio' ),
			'edit_item'         => __( 'Edit Occupation', 'studio' ),
			'update_item'       => __( 'Update Occupation', 'studio' ),
			'add_new_item'      => __( 'Add New Occupation', 'studio' ),
			'new_item_name'     => __( 'New Occupation Name', 'studio' ),
			'menu_name'         => __( 'Occupations', 'studio' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'client_occupation',
			'with_front' => false ),
		);

		register_taxonomy( 'client_occupation', 'studio_clients', $args );
	}
    // register custom taxonomy for clients post type end 
    
   function wpa_course_post_link( $post_link, $id = 0 ){
	    $post = get_post($id);  
	    if ( is_object( $post ) ){
	        $terms = wp_get_object_terms( $post->ID, 'client_occupation' );
	        if( $terms ){
	            return str_replace( '%client_occupation%' , $terms[0]->slug , $post_link );
	        }
	    }
	    return $post_link;  
	}
	add_filter( 'post_type_link', 'wpa_course_post_link', 1, 3 );


	function custom_post_type() {
	// Set UI labels for Custom Post Type
	    $labels = array(
	        'name'                => _x( 'Clients', 'Post Type General Name', 'studio' ),
	        'singular_name'       => _x( 'Client', 'Post Type Singular Name', 'studio' ),
	        'menu_name'           => __( 'Clients', 'studio' ),
	        'parent_item_colon'   => __( 'Parent Clients', 'studio' ),
	        'all_items'           => __( 'All Clients', 'studio' ),
	        'view_item'           => __( 'View Clients', 'studio' ),
	        'add_new_item'        => __( 'Add New Clients', 'studio' ),
	        'add_new'             => __( 'Add New', 'studio' ),
	        'edit_item'           => __( 'Edit Client', 'studio' ),
	        'update_item'         => __( 'Update Clients', 'studio' ),
	        'search_items'        => __( 'Search Clients', 'studio' ),
	        'not_found'           => __( 'Not Found', 'studio' ),
	        'not_found_in_trash'  => __( 'Not found in Trash', 'studio' ),
	    );
	     
	// Set other options for Custom Post Type	     
	    $args = array(
	        'label'               => __( 'studio_clients', 'studio' ),
	        'description'         => __( 'Clients', 'studio' ),
	        'labels'              => $labels,
	        // Features this CPT supports in Post Editor
	        'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', ),
	        /* A hierarchical CPT is like Pages and can have
	        * Parent and child items. A non-hierarchical CPT
	        * is like Posts.
	        */ 
	        'hierarchical'        => false,
	        'public'              => true,
	        'show_ui'             => true,
	        'show_in_menu'        => true,
	        'show_in_nav_menus'   => true,
	        'show_in_admin_bar'   => true,
	        'menu_position'       => 3,
	        'can_export'          => true,  
	        'exclude_from_search' => false,
	        'publicly_queryable'  => true,
	        'capability_type'     => 'page',
	         // You can associate this CPT with a taxonomy or custom taxonomy. 
	        'taxonomies'          => array( 'client_occupation' ),
	        // rewrite link adress
	        'rewrite' => array('slug' => 'studio_clients/%client_occupation%'),
	        'has_archive'         => 'studio_clients',
	    );

    // Set UI labels for Custom Post Type

	    $labels2 = array(
	        'name'                => _x( 'Stories', 'Post Type General Name', 'studio' ),
	        'singular_name'       => _x( 'Story', 'Post Type Singular Name', 'studio' ),
	        'menu_name'           => __( 'Stories', 'studio' ),
	        'parent_item_colon'   => __( 'Parent Stories', 'studio' ),
	        'all_items'           => __( 'All Stories', 'studio' ),
	        'view_item'           => __( 'View Stories', 'studio' ),
	        'add_new_item'        => __( 'Add New Stories', 'studio' ),
	        'add_new'             => __( 'Add New', 'studio' ),
	        'edit_item'           => __( 'Edit Stories', 'studio' ),
	        'update_item'         => __( 'Update Stories', 'studio' ),
	        'search_items'        => __( 'Search Stories', 'studio' ),
	        'not_found'           => __( 'Not Found', 'studio' ),
	        'not_found_in_trash'  => __( 'Not found in Trash', 'studio' ),
	    );

    // Set other options for Custom Post Type
	     
	    $args2 = array(
	        'label'               => __( 'stories', 'studio' ),
	        'description'         => __( 'Stories', 'studio' ),
	        'labels'              => $labels2,
	        // Features this CPT supports in Post Editor
	        'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', ),
	        /* A hierarchical CPT is like Pages and can have
	        * Parent and child items. A non-hierarchical CPT
	        * is like Posts.
	        */ 
	        'hierarchical'        => false,
	        'public'              => true,
	        'show_ui'             => true,
	        'show_in_menu'        => true,
	        'show_in_nav_menus'   => true,
	        'show_in_admin_bar'   => true,
	        'menu_position'       => 6,
	        'can_export'          => true,  
	        'exclude_from_search' => false,
	        'publicly_queryable'  => true,
	        'capability_type'     => 'page',
	        'taxonomies'          => array( 'category' ),
	    );
	     
	    // Registering your Custom Post Type
	    register_post_type( 'studio_clients', $args );
	    register_post_type( 'stories', $args2 );
	 
	}
	 
	/* Hook into the 'init' action so that the function
	* Containing our post type registration is not 
	* unnecessarily executed. 
	*/
	add_action( 'init', 'custom_post_type', 0 );
	// Hooking up our function to theme 
	// registering new post type end

	// adding custom post types to category page start
	add_filter('pre_get_posts', 'query_post_type');
	function query_post_type($query) {
	  if( is_category() ) {
	    $post_type = get_query_var('post_type');
	    if($post_type)
	        $post_type = $post_type;
	    else
	        $post_type = array('nav_menu_item', 'post', 'stories'); // don't forget nav_menu_item to allow menus to work!
	    $query->set('post_type',$post_type);
	    return $query;
	    }
	}

	// adding custom post types to category page start

	// adding post formats to studio_clients
    function add_post_format_to_studio_clients(){
		add_post_type_support('studio_clients', 'post-formats');
		add_post_type_support('stories', 'post-formats');
	}
    add_action( 'init', 'add_post_format_to_studio_clients');

    // change default url for custom post start(like uncategorized for regular posts)
    function default_taxonomy_term( $post_id, $post ) {
	    if ( 'publish' === $post->post_status ) {
	        $defaults = array(
	            'client_occupation' => array( 'Other'),
	            );
	        $taxonomies = get_object_taxonomies( $post->post_type );
	        foreach ( (array) $taxonomies as $taxonomy ) {
	            $terms = wp_get_post_terms( $post_id, $taxonomy );
	            if ( empty( $terms ) && array_key_exists( $taxonomy, $defaults ) ) {
	                wp_set_object_terms( $post_id, $defaults[$taxonomy], $taxonomy );
	            }
	        }
	    }
	}
	add_action( 'save_post', 'default_taxonomy_term', 100, 2 );
    // change default url for custom post end

    // registering new post type clients end
    
    //adding css styles
	function addStylesJs(){
		wp_enqueue_script('jq', get_template_directory_uri() . '/js/jquery-3.3.1.min.js', array(), 1.0, false);
		wp_enqueue_style('style', get_theme_file_uri( '/css/main.css'));
		wp_enqueue_style('style-sm', get_theme_file_uri( '/css/sm.css'), array(), '1.0', 'screen and (min-width: 576px)');
		wp_enqueue_style('style-md', get_theme_file_uri( '/css/md.css'), array(), '1.0', 'screen and (min-width: 768px)');
		wp_enqueue_style('style-lg', get_theme_file_uri( '/css/lg.css'), array(), '1.0', 'screen and (min-width: 992px)');
		wp_enqueue_style('style-xl', get_theme_file_uri( '/css/xl.css'), array(), '1.0', 'screen and (min-width: 1200px)');
		wp_enqueue_style('style-xxl', get_theme_file_uri( '/css/xxl.css'), array(), '1.0', 'screen and (min-width: 1650px)');
		wp_enqueue_style('style-xxxl', get_theme_file_uri( '/css/xxxl.css'), array(), '1.0', 'screen and (min-width: 1850px)');
		wp_enqueue_script('main_js', get_template_directory_uri() . '/js/main.min.js', array(), 1.0, true);
	}
	add_action('wp_enqueue_scripts', 'addStylesJs');

	//Controll post length on the main page

	function set_excerpt_length(){
		return has_post_thumbnail() ? 35 : 75;
	}
	add_filter('excerpt_length','set_excerpt_length');

	function custom_excerpt_read_more( $more ) {
		global $post;
	    return sprintf( '<a class="read-more" href="%1$s">%2$s</a>',
	        get_permalink( get_the_ID() ),
	        __( '<span class="read-more__icon company comp-circle-right"></span>', 'studio' )
	    );
	}
	add_filter( 'excerpt_more', 'custom_excerpt_read_more' );

	//widgets locations start
	function wpb_init_widgets($id){
		register_sidebar(array(
		    'name'=>'Search',
		    'id'=> 'search',
		    'before_widget'=> '<div class="search_top">',
		    'after_widget'=> '</div>'
		));
		register_sidebar(array(
		    'name'=>'Home bottom pictures slider',
		    'id'=> 'bottom_pictures_slider',
		    'before_widget'=> '<div class="portrait_slider_cnt__inner">',
		    'after_widget'=> '</div>'
		));
	}
	add_action('widgets_init', 'wpb_init_widgets');
	//widgets locations end

	//search input start
    function wpdocs_after_setup_theme() {
	    add_theme_support( 'html5', array( 'search-form' ) );
	}
	add_action( 'after_setup_theme', 'wpdocs_after_setup_theme' );
    //search input end

	// removing url field in comments section start
	function disable_url_in_comments_field($fields){
		if(isset($fields['url']))
		unset($fields['url']);
		return $fields;
	}
    add_filter('comment_form_default_fields', 'disable_url_in_comments_field');
    // removing url field in comments section end
    /**
	 * Hides the custom post template for pages on WordPress 4.6 and older
	 *
	 * @param array $post_templates Array of page templates. Keys are filenames, values are translated names.
	 * @return array Filtered array of page templates.
	 */
	function makewp_exclude_page_templates( $post_templates ) {
	    if ( version_compare( $GLOBALS['wp_version'], '4.7', '&lt;' ) ) {
	        unset( $post_templates['templates/my-full-width-post-template.php'] );
	    }
	  
	    return $post_templates;
	}
	// First, create a function that includes the path to your favicon
	function add_favicon() {
	  	$favicon_url = get_template_directory_uri() . '/images/admin-favicon.ico';
		echo '<link rel="shortcut icon" href="' . $favicon_url . '" type="image/x-icon" sizes="32x32" />';
	}
	  
	// Now, just make sure that function runs when you're on the login page and admin pages  
	add_action('login_head', 'add_favicon');
	add_action('admin_head', 'add_favicon');

    // custom meta boxes in admin pannel start

    // in case metabox plugin is deactivated start

    if ( ! function_exists( 'rwmb_meta' ) ) {
	    function rwmb_meta( $key, $args = '', $post_id = null ) {
	        return false;
	    }
	}

    // in case metabox plugin is deactivated end

	function your_prefix_get_meta_box( $meta_boxes ) {
        $prefix = 'studio_';

		$meta_boxes[] = array(
			'id' => 'description',
			'title' => esc_html__( 'Short client description', 'studio' ),
			'post_types' => array( 'studio_clients' ),
			'context' => 'after_title',
			'priority' => 'default',
			'autosave' => false,
			'fields' => array(
				array(
				    'id'          => $prefix . 'text',
				    'placeholder'        => 'Short description goes here',
				    'type'        => 'text',
				    'size'        =>  '60',
				    'attributes' => array(
						'minlength' => '4',
						'maxlength' => 30,
					),
				),
			),
		);

		$meta_boxes[] = array(
			'id' => 'social_links',
			'title' => esc_html__( 'Fill out social profile links', 'studio' ),
			'post_types' => array( 'studio_clients' ),
			'context' => 'normal',
			'priority' => 'default',
			'autosave' => false,
			'fields' => array(
				array(
					'id' => $prefix . 'url_1',
					'type' => 'url',
					'name' => esc_html__( 'Your Facebook Link', 'studio' ),
					'class' => 'icons',
				),
				array(
					'id' => $prefix . 'url_2',
					'type' => 'url',
					'name' => esc_html__( 'Your Instagram Link', 'studio' ),
					'class' => 'icons',
				),
				array(
					'id' => $prefix . 'url_3',
					'type' => 'url',
					'name' => esc_html__( 'Your Youtube Link', 'studio' ),
					'class' => 'icons',
				),
				array(
					'id' => $prefix . 'url_4',
					'type' => 'url',
					'name' => esc_html__( 'Your Twitter Link', 'studio' ),
					'class' => 'icons',
				),
			),
		);

		$meta_boxes[] = array(
			'id' => 'featured',
			'title' => esc_html__( 'Featured', 'studio' ),
			'post_types' => array( 'stories' ),
			'context' => 'after_editor',
			'priority' => 'default',
			'autosave' => true,
			'fields' => array(
				array(
					'id' => $prefix . 'featured_1',
					'name' => esc_html__( 'Featured this post', 'studio' ),
					'type' => 'checkbox',
					'desc' => esc_html__( 'If this box is checked and featured image is set the post will be displayed on home page as featured(only the last featured post will be displayed)', 'studio' ),
				),
			),
		);
		return $meta_boxes;
	}
	add_filter( 'rwmb_meta_boxes', 'your_prefix_get_meta_box' );
    // custom meta boxes in admin pannel end
    
    // adding custom classes to body
    function my_plugin_body_class($classes) {
	    if(is_page_template( 'page-all-news.php' )){ 
	    	$classes[] = 'all_news_page'; 
	    } else if(is_page_template( 'page-about.php' )){
	    	$classes[] = 'page_about'; 
	    } else if(is_page_template( 'page-all-stories.php' )){
	    	$classes[] = 'all_stories_page'; 
	    }
	    return $classes;
	}

	add_filter('body_class', 'my_plugin_body_class');

	// Add responsive container to embeds
 
	function alx_embed_html( $html ) {
	   return '<div class="oembed_container">' . $html . '</div>';
	}
	add_filter( 'embed_oembed_html', 'alx_embed_html', 10, 3 );

	// right pagination to main category loop
	function my_post_queries( $query ) {
	  // do not alter the query on wp-admin pages and only alter it if it's the main query
	  if (!is_admin() && $query->is_main_query()){

	    if(is_category()){
			$query->set('posts_per_page', 14);
			$query->set('meta_query', array( 
			    array(
			        'key' => '_thumbnail_id'
			    ) 
			));
	    }
	    if (is_search()) {
	    	$query->set('post_type',array('post', 'stories', 'studio_clients'));
	    	$query->set('posts_per_page', 14);
	    }

	  }
	}
	add_action( 'pre_get_posts', 'my_post_queries' );

	// check for main loop pagination start

	function is_paginated() {
	    global $wp_query;
	    if ( $wp_query->max_num_pages > 1 ) {
	        return true;
	    } else {
	        return false;
	    }
	}

	// check for main loop pagination end