<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<title>
	    <?php bloginfo('name');?> |
	    <?php is_front_page() ? bloginfo('description'): 
        wp_title('');
	    ?> 
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="Igor Barabash">
    <meta name="keywords" content="custom wordpress theme">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/site.webmanifest">
	<link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">
	<?php wp_head();?>
</head>


<body <?php body_class();?> >

	<div class="container position-absolute main_navigation_cnt">
		<div class="row">
			<nav class="d-flex justify-content-between flex-row main_navigation <?php if( is_page('About')): ?>about_page_search <?php endif; ?>">
				<div class="d-inline-flex main_navigation__button_cnt">
					<button id="menu_button">
						<svg class="icon th-menu-outline"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#th-menu-outline"></use></svg>
					</button>
					<ul class="p-0 mb-0 d-inline-flex">
						<li class="d-flex align-items-center top_menu_links"><a href="<?php echo get_page_link( get_page_by_path( 'all-clients' )->ID ); ?>">CLIENTS</a></li>
						<li class="d-flex align-items-center top_menu_links"><a href="<?php echo get_page_link( get_page_by_path( 'all-news' )->ID ); ?>">NEWS</a></li>
					</ul>
				</div>
				<?php  if(is_active_sidebar('search')):?>
				<div class="position-relative search_cnt">
					<?php dynamic_sidebar('search'); ?>   
				</div>
				<?php endif; ?>
			</nav>
		</div>
	</div>

	<div class="main_menu fadeOutLeft" id="menu">
		<button id="close_menu" class="bounceOut"><svg class="icon close-cross"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#close-cross"></use></svg></button>
		<div class="main_menu__inner">
			<?php if ( has_nav_menu( 'main_menu' ) ) : ?>
			<div class="d-flex justify-content-center align-items-center">
				<?php
			       wp_nav_menu( array(
			           'menu'              => 'Main menu',
			           'theme_location'    => 'main_menu',
			           'menu_class'        => 'main_menu_list_cnt__list',
			           'depth'             => 1,
			           'container'         => 'div',           
			           'container_class'   => 'main_menu_list_cnt',           
				       )
			       );
			    ?>
		    </div>
			<?php endif; ?>
        </div>
	</div>

	<div class="site_wrap <?php if(is_single() || is_search()){ ?>single_post_seach_bg <?php } elseif(is_404()){ ?>single_post_seach_bg <?php } ?>">
