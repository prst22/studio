<?php
/*
Template Name: page 404
*/
?>
<?php
get_header(); ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
            	<div class="page-404 d-flex justify-content-center align-items-center">
            		<h1>Something went wrong 404</h1>
            	</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer();