<!-- get content part -->
<?php if(is_page('all-news')): ?>
	<div class=" col-12 col-lg-6 w-100 p-0 grid_item">
		<figure class="news_stories_thumb_cnt">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail('small-thumnail'); ?>
			</a>
		</figure>
			
	    <h5 class="position-relative px-2 px-sm-3 mb-3 grid_item__heading">
	    	<a href="<?php the_permalink(); ?>">
	    	<?php
			    if (strlen($post->post_title) > 30)
			       echo substr($post->post_title, 0, 30) . ' ...';
			    else
			       echo $post->post_title; 
			   ?>

	    	</a>
	    	<span class="news_stories_date"><?php the_time('j F, Y'); ?></span>

	    </h5>
	</div>
<?php elseif (is_page('all-stories')): ?>

    <div class=" col-12 col-lg-6 w-100 p-0 grid_item">
		<figure class="news_stories_thumb_cnt">
			<a  class="position-relative d-block all_stories_l" href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail('small-thumnail'); ?>
				<div class="position-absolute d-flex justify-content-center align-items-center overlay">
					<svg class="icon play"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#play"></use></svg>
				</div>
			</a>
		</figure>
			
	    <h5 class="position-relative px-2 px-sm-3 mb-3 grid_item__heading grid_item__heading--all_stories">
	    	<a href="<?php the_permalink(); ?>">
	    	<?php
			    if (strlen($post->post_title) > 30)
			       echo substr($post->post_title, 0, 30) . ' ...';
			    else
			       echo $post->post_title; 
			   ?>

	    	</a>
	    	<span class="news_stories_date"><?php the_time('j F, Y'); ?></span>

	    </h5>
	</div>
<?php endif; ?>